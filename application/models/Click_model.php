<?php
class Click_model extends CI_Model {

    private $CLICKTABLE = 'e_hist';
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    /* 날짜와 타입을 기준으로 같은날 클릭하면 hit가 증가하고, 최초 클릭했을 땐 row가 생성됨*/
    public function save($data = array())
    {

        $insert_data = array();
        $insert_data['etype']  = $data['etype'];
        $insert_data['userid']    = $data['userid'];

        $insert_result = $this->db->insert($this->CLICKTABLE,$insert_data);
        if ( $insert_result ) {
            return $this->db->insert_id();
        }
    }

    /* 같은날 클릭한 처리에 대해선 hit를 더해준다 */
    public function check_today_click ($ip,$date,$type)
    {
        return $this->db->select('id,hit')
            ->from($this->CLICKTABLE)
            ->where(array(
            'date(created_at)'=>$date,
            'ip'=>$ip,
            'type'=>$type
        ))->get()->row_array();
    }
}

