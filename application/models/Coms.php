<?php
/*
 * com_id[pk]
 * com_name
 * phone
 * use_yn
 * reg_dtm
 * */
class Coms extends CI_Model {
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->model('coms');
    }
    /*
     * 회사정보 등록
     * */
    public function r()
    {
        $this->load->library('form_validation');

        $result = array();
        $result['code'] = '';
        $result['msg'] = array();

        $this->form_validation->set_rules('com_name','','required|min_length[2]|max_length[30]|trim',array(
            'required' => '회사명은 반드시 입력해야 합니다.',
            'min_length' => '회사명은 2자 이상 입력해주세요',
            'max_length' => '회사명은 30자 이상을 넘을 수 없습니다.'
        ));
        $this->form_validation->set_rules('phone','','required|min_length[11]|max_length[15]|trim',array(
            'required' => '연락처는 필수 입니다.',
            'min_length' => '잘못된 번호 형식 입니다',
            'max_length' => '잘못된 번호 형식 입니다'
        ));

        if ( $this->form_validation->run() ) {
            $saveData = array();
            $saveData['com_name'] = $this->input->post('com_name');
            $saveData['phone'] = $this->input->post('phone');
            $saveData['use_yn'] = $this->input->post('use_yn');
            if ($this->coms->save($saveData) ) {
                $result['code'] = '0000';
                $result['msg'] = array('result'=>'success');
            }else{
                $result['code'] = '1111';
                $result['msg'] = array('result'=>'저장실패');
            }
        }else{
            $result['msg'] = $this->form_validation->error_array();
        }

        echo json_encode($result);
    }

    public function m()
    {
        $this->load->library('form_validation');

        $result = array();
        $result['code'] = '';
        $result['msg'] = array();

        $this->form_validation->set_rules('com_name','','required|min_length[2]|max_length[30]|trim',array(
            'required' => '회사명은 반드시 입력해야 합니다.',
            'min_length' => '회사명은 2자 이상 입력해주세요',
            'max_length' => '회사명은 30자 이상을 넘을 수 없습니다.'
        ));
        $this->form_validation->set_rules('phone','','required|min_length[11]|max_length[15]|trim',array(
            'required' => '연락처는 필수 입니다.',
            'min_length' => '잘못된 번호 형식 입니다',
            'max_length' => '잘못된 번호 형식 입니다'
        ));

        if ( $this->form_validation->run() ) {
            $updateData = array();
            $updateData['com_id'] = $this->input->post('com_id');
            $updateData['com_name'] = $this->input->post('com_name');
            $updateData['phone'] = $this->input->post('phone');
            $updateData['use_yn'] = $this->input->post('use_yn');
            if ($this->coms->update($updateData,$updateData['com_id']) ) {
                $result['code'] = '0000';
                $result['msg'] = array('result'=>'success');
            }else{
                $result['code'] = '1111';
                $result['msg'] = array('result'=>'저장실패');
            }
        }else{
            $result['msg'] = $this->form_validation->error_array();
        }

        echo json_encode($result);
    }

}

