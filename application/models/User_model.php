<?php
class User_model extends CI_Model {

    private $USER = 'users';
    private $ADMIN = 'admin';
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();

    }

    public function r($limit=30,$offset=0,$search=array())
    {
        $return = array();

        $this->db->start_cache();
        if ( isset($search['userid']) ) {
            $this->db->where('userid',$search['userid']);
        }
        if ( isset($search['user_name']) ) {
            $this->db->where('user_name',$search['user_name']);
        }
        $this->db->stop_cache();
        $return['total'] = $this->db->count_all_results($this->USER);
//        $this->db->select('userid,user_name,userid');
        $this->db->limit($limit);
        $this->db->offset($offset);
        $this->db->order_by('userid','desc');
        $result = $this->db->get($this->USER);
        $return['list'] = $result->result_array();

        $this->db->flush_cache();
        return $return;
    }
    /* 사용자 생성 */
    public function c($user = array())
    {

        $this->load->model('images');

        $insert = array();
        $insert['userid'] = $user['userid'];
        $insert['comid'] = $user['comid'];
        $insert['user_type'] = $user['user_type'];
        $insert['user_name'] = $user['user_name'];
        $insert['user_phone'] = $user['user_phone'];
        $insert['user_ktlink'] = $user['user_ktlink'];
        $insert['com_type'] = $user['com_type'];
        $insert['com_cd'] = $user['com_cd'];
        $insert['com_name'] = $user['com_name'];
        $insert['com_addr'] = $user['com_addr'];
        $insert['com_detail'] = $user['com_detail'];

        $insert['com_photo1'] = $user['com_photo1'];

        //이미지가 있으면 이미지를 처리 해야 합니다.

        $this->db->insert($insert);
    }
    /* 사용자 업데이트 함수 */
    public function u($user_id, $user = array())
    {
        if ( !empty($user)) {
            if ( $this->db->where('id',$user_id)->update($this->USER,$user) ) {
                return $user_id;
            }
        }
        return false;
    }


    /* 사용자 삭제 함수 */
    public function D($user_id)
    {
        return $this->db->where('id',$user_id)->delete($this->USER);
    }

    public function save_images()
    {

    }

    public function get($userid)
    {
        $return = array();
        $this->db->where('userid',$userid);
        $return['content'] = $this->db->get($this->USER)->row();
        if ($return['content']) {

        }
    }

    public function get_shop($com_id)
    {
        $this->db->where('comid',$com_id);
        return $this->db->get($this->USER)->row_array();
    }

    public function admin_check($userid,$userpw){
        return $this->db->where(array(
            'userid'=> "'{$userid}'",
            'passwd' => "password('{$userpw}')"
        ),'',false)->get($this->ADMIN)->num_rows();
    }
}

