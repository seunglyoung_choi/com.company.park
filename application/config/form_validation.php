<?php
$config = array(
    'user/ajaxCreate' => array(
        array(
            'field' => 'userid',
            'label' => '사용자 아이디',
            'rules' => 'required|min_length[10]',
            'errors'=> array(
                'required'=>'사용자 아이디는 필수 입력 입니다',
                'min_length'=>'아이디는 최소2자 이상 입니다'
            )
        )
    ),
    'user/login' => array(
        array(
            'field' => 'userid',
            'label' => '사용자 아이디',
            'rules' => 'required|trim|min_length[2]|max_length[20]',
            'errors'=> array(
                'required'=>'사용자 아이디는 필수 입력 입니다',
                'min_length'=>'아이디는 최소2자 이상 입니다',
                'max_length'=>'아이디는 최대20자 이하 입니다'

            )
        ),
        array(
            'field' => 'passwd',
            'label' => '비밀번호',
            'rules' => 'required|trim',
            'errors'=> array(
                'required'=>'비밀번호를 입력해주세요'

            )
        )
    )
);