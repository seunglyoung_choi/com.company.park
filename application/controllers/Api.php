<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('click_model');
    }

    public function checkClick()
    {
        $clickType = $this->input->post('etype');
        $userid = $this->input->post('userid');

        $data = array();
        $data['etype'] = $clickType;
        $data['userid'] = $userid;
        if($clickType) {
            $data['result_insert'] = $this->click_model->save($data);
        }
        echo 'OK';
    }

    public function login()
    {
        $this->load->model('user_model');
        $this->load->library('form_validation');
        $result = array();
        $result['code'] = '';
        $result['msg'] = '';
        $result['redirect_url'] = '/admin/main';

        $userIdMinSize = 5;
        $userIdMaxSize = 20;

        $userid = $this->input->post("userid");
        $userpw = $this->input->post("passwd");

        $result['is_success'] = $this->form_validation->run();

        $check_pw = $this->user_model->admin_check($userid,$userpw);

        if ( $check_pw == 0 ) {
            $result['is_success'] = false;
            $result['code'] = '9999';
            $result['msg'] = array('passwd'=>'비밀번호를 정확히 입력해주세요.');
        }

        if( $result['is_success'] ) {
            $result['code'] = '0000';
            $result['msg'] = array('userid'=>'로그인 되었습니다.');
            $sessionConfig = array(
                'userid'=> $userid,
                'is_login'=>'Y',
                'login_time' => date('Y-m-d H:i:s')
            );
            $this->session->set_userdata($sessionConfig);
        } else {
            $result['msg'] = $this->form_validation->error_array();
        }
        echo json_encode($result);
    }

    public function test()
    {
        $localFileName = APPPATH.'views/testfile.php';
        $lastModified = date('D, d M Y H:i:s T', filemtime($localFileName));
        header('Cache-Control: private, must-revalidate, max-age=32849234892');
        header('Last-Modified: '.$lastModified);

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($localFileName) )
        {
            header('HTTP/1.0 304 Not Modified');
            exit;
        }else{
            $data['tesst'] = '이건 asdfasfasdf반영 될가요?';
            $this->load->view('testfile',$data);
        }

    }


}
