<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    private $COM = 'coms';

    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {

    }

    public function make()
    {

        $data = array();
        $this->common->av('user/make',$data);
    }

    public function save($com)
    {
        return $this->db->insert($this->COM,$com);
    }

    public function update($com,$com_id)
    {
        if( $this->db->where('com_id',$com_id)->update($this->COM,$com) ) {
            return $com_id;
        }
        return false;
    }
}
