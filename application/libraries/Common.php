<?php
class Common {

    private $CI;
    private $ADMINPATH = 'admin/';

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function av($page,$data = array())
    {
        if( empty($data) ) {
            $this->CI->load->view($this->ADMINPATH.'inc/header');
            $this->CI->load->view($this->ADMINPATH.$page);
            $this->CI->load->view($this->ADMINPATH.'inc/footer');
        } else {
            $this->CI->load->view($this->ADMINPATH.'inc/header',$data);
            $this->CI->load->view($this->ADMINPATH.$page,$data);
            $this->CI->load->view($this->ADMINPATH.'inc/footer',$data);
        }

    }
}