<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>골드라인</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta name="author" content="Stoyview"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="keywords" content="골드라인 음주가무" />
    <meta name="description" content="골드라인 음주가무" />

    <link rel="stylesheet" href="/css/style.css" type="text/css" />
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
</head>
<body>
<div id="wrap">
    <div id="header">
        <div class="top_btn"><a href="/" class="btn_home"></a></div>
        <h1><a href="/"><img src="/images/top_logo.png" width="100%"></a></h1>
    </div>
    <div id="sb_top">
        <div class="ttl"><?php echo $com['com_name']?></div>
        <div class="info"><div class="txt1"><?php echo MODE_TYPE[$com['com_type']]?> / <?php echo $com['com_name']?> / <?php echo $com['user_name']?></div><div class="txt2"><?php echo $com['com_addr']?></div></div>
    </div>ㅎ
    <div id="body">
        <div id="sb_con">
            <?php echo $com['com_detail'] ?>
        </div>
        <div id="btn_area">
            <ul>
                <li><a href="#" class="btn1 call_com"><img src="/images/m_call.png" width="15px;" align="absmiddle">전화상담</a></li>
                <li><a href="/" class="btn2">목록보기</a></li>
                <li><a href="<?php echo $com['kakao_link']?>" class="btn3"><img src="/images/m_talk.png" width="15px;" align="absmiddle">카톡상담</a></li>
            </ul>
        </div>
    </div>
    <p class="pdt30"></p>
    <div id="footer">
        <div class="tab">
            <ul>
                <li class="on">골드라인소개</li>
                <li class="on">여성회원</li>
                <li class="on">업소회원</li>
            </ul>
        </div>
        <div class="box1">본 서비스는 여알바 혹은 여직원을 구하는 인사담당자와 구직을 원하는 여성분들을 연결해주는 서비스입니다.<br>
            <p class="pdt10"></p>
            <div class="btn"><a href="<?php echo $com['kakao_link']?>" class="bg">카카오톡 상담하기</a></div>
            <div>
            </div>
        </div>
        <script>
            $('.call_com').on("click",function(e) {
                var comid = $(this).data('comid');
                $.post('/api/get_phone_number', {param_comid:comid} ,function(data}
                {

                })
                e.preventDefault();
            })
        </script>
</body>
</html>