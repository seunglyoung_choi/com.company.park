<section class="container">
    <div class="row">
        <h1>사용자 등록</h1>
    </div>
    <div class="row">
        <form class="col s12">
            <div class="row">
                <div class="col s12">
                    <p>사용자 타입</p>
                    <div class="col s4">
                        <input name="user_type" type="radio" id="user_type" value="0" checked/>
                        <label for="user_type">무료</label>
                    </div>
                    <div class="col s4">
                        <input name="user_type" type="radio" id="user_type1" value="1"/>
                        <label for="user_type1">유료</label>
                    </div>
                    <div class="col s4">
                        <input name="user_type" type="radio" id="user_type2" value="2"/>
                        <label for="user_type2">스페셜</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <p>업소타입</p>
                    <div class="col s4">
                        <input name="com_type" type="radio" id="com_type" value="S" checked/>
                        <label for="com_type">S</label>
                    </div>
                    <div class="col s4">
                        <input name="com_type" type="radio" id="com_type1" value="N"/>
                        <label for="com_type1">N</label>
                    </div>
                    <div class="col s4">
                        <input name="com_type" type="radio" id="com_type2" value="H"/>
                        <label for="com_type2">H</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s4">
                    <input placeholder="골드문" id="user_name" name="user_name" type="text" class="validate">
                    <label for="first_name">관리자이름</label>
                </div>
                <div class="input-field col s4">
                    <input placeholder="업소이름" id="kakao_link" name="kakao_link" type="text" class="validate">
                    <label for="kakao_link">카카오링크</label>
                </div>
                <div class="input-field col s4">
                    <input placeholder="01000000000" id="user_phone" name="user_phone" type="text" class="validate">
                    <label for="user_phone">연락처</label>
                </div>
            </div>
            <div class="row">

                <div class="input-field col s4">
                    <input placeholder="Bar" id="com_cd" name="com_cd" type="text" class="validate">
                    <label for="com_cd">업소cd</label>
                </div>
                <div class="input-field col s4">
                    <input placeholder="HO바" id="com_name" name="com_name" type="text" class="validate">
                    <label for="user_phone">업소이름</label>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field s6">
                    <div class="btn">
                        <span>대표이미지</span>
                        <input type="file" name="com_photo1">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Upload one or more files">
                    </div>
                </div>
                <div class="input-field col s6">
                    <input placeholder="서울시 강남구 논현동 무슨1바길 15" id="com_addr" name="com_addr" type="text" class="validate">
                    <label for="com_addr">업소주소</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="com_detail" name="com_detail" class="materialize-textarea"></textarea>
                    <label for="com_detail">상세정보</label>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field col12">
                    <div class="btn">
                        <span>File</span>
                        <input type="file" name="images" multiple>
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="다중 이미지 파일">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <button class="btn">저장</button>
                </div>
            </div>
        </form>
    </div>
</section>