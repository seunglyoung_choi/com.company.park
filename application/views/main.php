<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>골드라인</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta name="author" content="Stoyview"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="keywords" content="골드라인 음주가무" />
    <meta name="description" content="골드라인 음주가무" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/asset/js/common.js"></script>
    <script src="/asset/js/main.js"></script>
</head>
<body>
<div id="wrap">
    <div id="header">
        <div class="top_btn"><a href="#" class="btn_home"></a></div>
        <h1><a href="#"><img src="images/top_logo.png" width="170px"></a></h1>
    </div>
    <div id="visual">
        <div class="round">
            <div class="rank1">목돈마련</div><div class="rank2">단기알바</div><div class="rank3">주말알바</div><div class="rank4">가라오케</div><div class="rank5">용돈벌이</div><div class="rank6">소프트</div><div class="rank7">여알바</div><div class="rank8">출퇴근자유</div><div class="rank9">수위조절</div><div class="rank10">토킹바</div>
        </div>
    </div>
    <div id="body">
        <div id="m_con1">
            <p class="ttl">Part</p>
            <p class="pdt10"></p>
            <div class="color_bar"></div>
            <div class="color_icon">
                <div class="icon1"><p class="bullet">ㆍ</p><img src="images/icon_soft.png" width="100%"><p class="color_txt">소프트</p></div>
                <div class="icon2"><p class="bullet">ㆍ</p><img src="images/icon_normal.png" width="100%"><p class="color_txt">노멀</p></div>
                <div class="icon3"><p class="bullet">ㆍ</p><img src="images/icon_hard.png" width="100%"><p class="color_txt">하드</p></div>
            </div>
        </div>
        <p class="pdt20"></p>
        <div id="m_con2">
            <p class="ttl">업소 스타일</p>
            <p class="pdt10"></p>
            <div class="list">
                <ul>
                    <?php if ($users['list']) {
                            foreach ( $users['list'] as $user ) {
                        ?>
                    <li>
                        <div class="s_pic"><div class="soft"></div><img src="images/<?php echo $user['com_photo1']?>" width="100%"></div>
                        <div class="txt_area"><p class="txt1"><?php echo $user['user_name']?></p><p class="txt2"><?php echo $user['com_name']?>/<?php echo $user['com_cd']?></p><p class="txt3"><?php echo $user['com_addr']?></p></div>
                        <div class="btn_area"><a href="/main/detail/<?php echo $user['userid']?>" class="btn_veiw detailBtn">상세보기</a><div class="btn_call"><a href="tel:<?php echo $user['user_phone']?>" class="btn_bg1 phoneBtn"></a></div><div class="btn_talk"><a href="<?php echo $user['kakao_link']?>" class="btn_bg2 kakaoBtn"></a></div></div>
                    </li>
                    <?php } } else {?>
                        <li>
                            <p>등록된 업소가 없습니다</p>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <p class="pdt30"></p>
    <div id="footer">
        <div class="tab">
            <ul>
                <li class="on">골드라인소개</li>
                <li class="on">여성회원</li>
                <li class="on">업소회원</li>
            </ul>
        </div>
        <div class="box1">본 사이트는 구인/구직 및 고객 홍보를 위한 서비스를 제공합니다.<br>
            필요하신 모든 서비스는 아래의 상담하기 버튼을 통해서 상담신청하세요.
            <p class="pdt10"></p>
            <div class="btn"><a href="https://open.kakao.com/o/s0iPqYv" class="bg">카카오톡 상담하기</a></div>
            <div>
            </div>
        </div>
</body>
</html>
