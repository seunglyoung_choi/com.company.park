jQuery(function($){
    var $phoneButton  = $('.phoneBtn');
    var $detailButton = $('.detailBtn');
    var $kakaoButton  = $('.kakaoBtn');
    var sendData = {};
    $phoneButton.on("click",function(e){

        sendData.etype = 'p';
        sendData.userid = '11';
        $.post('/api/checkClick',sendData,function(){
            window.location.href = this.href;
        })
        e.preventDefault();
    })

    $kakaoButton.on("click",function(e){
        sendData.etype = 'k';
        sendData.userid = '11';
        $.post('/api/checkClick',sendData,function(){
            window.location.href = this.href;
        })
        e.preventDefault();
    })

    $detailButton.on("click",function(e){
        sendData.etype = 'd';
        sendData.userid = '11';
        $.post('/api/checkClick',sendData,function(){
            window.location.href = this.href;
        })
        e.preventDefault();
    })
})